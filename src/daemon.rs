use std::{
	io::Write,
	net::SocketAddr,
	os::fd::{
		AsRawFd,
		FromRawFd,
		IntoRawFd,
	},
	path::{
		Path,
		PathBuf,
	},
	process::Command,
};

use anyhow::Context;
use tokio::io::{
	unix::AsyncFd,
	Interest,
};

use crate::{
	config::{
		self,
		PortForward,
	},
	exec,
	forkpipe::{
		ForkPipe,
		ForkPipeEnd,
	},
	recursive_delete,
	AnyhowErrorFmt,
	ProcError,
};

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub enum P2C {
	InterfaceMoved,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub enum C2P {
	SetupFinished,
}

pub fn daemon(name: String, cfg_path: PathBuf) -> anyhow::Result<()> {
	if uapi::getuid() != 0 {
		// make sure only root runs the daemon, even if the executable is suid
		anyhow::bail!("Daemon must be ran as root");
	}

	// mask termination signals - we handle them with signalfd
	let mut signals = uapi::empty_sig_set()?;
	uapi::sigaddset(&mut signals, uapi::c::SIGTERM)?;
	uapi::sigaddset(&mut signals, uapi::c::SIGINT)?;
	uapi::sigaddset(&mut signals, uapi::c::SIGCHLD)?;
	uapi::pthread_sigmask(uapi::c::SIG_BLOCK, Some(&signals), None)?;

	// create signalfd
	let signalfd =
		uapi::signalfd_new(&signals, uapi::c::SFD_CLOEXEC).context("While creating signalfd")?;

	log::info!("Reading config from {:?}", cfg_path);
	let cfg = std::fs::read_to_string(&cfg_path)
		.with_context(|| format!("While reading config file {:?}", cfg_path))?;
	let cfg = toml::from_str::<config::Config>(&cfg)
		.with_context(|| format!("While parsing config file {:?}", cfg))?;
	if cfg.permissions > 0o777 {
		anyhow::bail!("Invalid permissions: {:#o}", cfg.permissions);
	}

	let state_dir = Path::new("/run/wg-ns/").join(name);
	log::debug!("Creating state directory at {:?}", state_dir);
	std::fs::create_dir_all(&state_dir).context("While creating state directory")?;
	scopeguard::defer!(recursive_delete(&state_dir));

	// set permissions
	// TODO: is there a way to do this atomically with dir creation?
	{
		log::debug!(
			"Setting state directory permissions to {}:{} {:#o}",
			cfg.uid,
			cfg.gid,
			cfg.permissions
		);
		let fd = uapi::open(
			state_dir.as_path(),
			uapi::c::O_RDONLY | uapi::c::O_DIRECTORY | uapi::c::O_CLOEXEC,
			0,
		)
		.context("While opening state directory")?;

		uapi::fchown(fd.as_raw_fd(), cfg.uid, cfg.gid)
			.context("While changing ownership of state directory")?;

		uapi::fchmod(fd.as_raw_fd(), cfg.permissions)
			.context("While changing permissions of state directory")?;
	}

	// open sockets
	let sockets = cfg
		.port_forwards
		.iter()
		.map(|pf| {
			let addr = pf.listen_addr();
			log::debug!("Opening socket on {}", addr);
			let listener = std::net::TcpListener::bind(addr)
				.with_context(|| format!("While creating tcp socket binding to {}", addr))?;
			listener
				.set_nonblocking(true)
				.context("While setting socket to non-blocking mode")?;
			let fd = uapi::OwnedFd::new(listener.into_raw_fd());
			log::debug!("DEBUG FD: {}", fd.as_raw_fd());
			Ok(fd)
		})
		.collect::<anyhow::Result<Vec<_>>>()?;

	// load wg config
	let wg_cfg_path = cfg_path.parent().unwrap().join(&cfg.wireguard_cfg);
	log::info!("Loading wireguard config at {:?}", wg_cfg_path);
	let mut ini = ini::Ini::load_from_file(&wg_cfg_path)
		.with_context(|| format!("While reading wireguard config {:?}", wg_cfg_path))?;
	let interface_section = ini
		.section_mut(Some("Interface"))
		.ok_or(anyhow::anyhow!("Missing Interface section"))?;

	let addresses = interface_section
		.remove("Address")
		.ok_or(anyhow::anyhow!("Missing Address entry"))?;
	let dns = interface_section
		.remove("DNS")
		.ok_or(anyhow::anyhow!("Missing DNS entry"))?;

	std::fs::write(
		state_dir.join("resolv.conf"),
		format!("nameserver {}\n", dns),
	)
	.context("While writing resolv.conf")?;

	// Create wg interface
	log::debug!("Creating wg interface {:?}", cfg.interface_name);
	exec(&[
		"ip",
		"link",
		"add",
		&cfg.interface_name,
		"type",
		"wireguard",
	])?;

	// Load basic wireguard config
	log::debug!("Loading wireguard config");
	let mut wg_cfg = vec![];
	ini.write_to(&mut wg_cfg)?;
	let mut wg_setconf = Command::new("wg")
		.args(&["setconf", &cfg.interface_name, "/dev/stdin"])
		.stdin(std::process::Stdio::piped())
		.spawn()
		.context("While spawning `wg`")?;
	wg_setconf.stdin.take().unwrap().write_all(&wg_cfg)?;
	wg_setconf.wait()?.error_for_status()?;

	// fork
	log::debug!("Forking child process in namespace");
	let fp = ForkPipe::<P2C, C2P>::new()?;
	let mut clone = clone3::Clone3::default();
	clone.flag_newnet();
	clone.flag_newns();
	clone.exit_signal(uapi::c::SIGCHLD as _);
	let child_pid = match unsafe { clone.call() }.context("While forking")? {
		0 => {
			// child. make sure this does not traverse up the call stack.
			// TODO: use std::panic::always_abort when stable
			std::panic::set_hook(Box::new(|pi| {
				eprintln!("child proc panicked: {}", pi);
				std::process::abort();
			}));

			if let Err(err) = child(fp.child_end(), &state_dir, cfg, addresses, sockets) {
				eprintln!("Error in child process:\n{}", AnyhowErrorFmt(err));
				std::process::exit(1);
			}
			std::process::exit(0);
		}
		child_pid => child_pid,
	};

	// parent - drop FDs that only the child uses
	let mut fp = fp.parent_end();
	std::mem::drop(sockets);

	// write pid
	log::info!(
		"Manager PID: {}, Child PID: {}",
		std::process::id(),
		child_pid
	);
	std::fs::write(state_dir.join("ns.pid"), child_pid.to_string())
		.context("While writing namespace PID")?;

	// setup veth
	for veth in cfg.veth.iter() {
		// Create veth interfaces
		log::debug!(
			"Creating veth interfaces {:?} and {:?}",
			veth.host_name,
			veth.container_name
		);
		exec(&[
			"ip",
			"link",
			"add",
			&veth.host_name,
			"type",
			"veth",
			"peer",
			"name",
			&veth.container_name,
			"netns",
			&child_pid.to_string(),
		])?;

		log::debug!("Configuring veth {:?}", veth.host_name);
		exec(&[
			"ip",
			"addr",
			"add",
			"dev",
			&veth.host_name,
			&veth.host_ip.to_string(),
			"scope",
			"link",
			"peer",
			&veth.container_ip.to_string(),
		])?;

		exec(&["ip", "link", "set", "dev", &veth.host_name, "up"])?;
	}

	// move into
	log::debug!("Moving wg interface to network namespace");
	exec(&[
		"ip",
		"link",
		"set",
		&cfg.interface_name,
		"netns",
		&child_pid.to_string(),
	])?;

	// tell child to continue
	fp.send(&P2C::InterfaceMoved)?;
	assert!(matches!(fp.recv()?, C2P::SetupFinished));

	// now all set
	if let Err(e) = sd_notify::notify(false, &[sd_notify::NotifyState::Ready]) {
		eprintln!("Could not set systemd state, ignoring. Error: {}", e);
	}
	log::info!("All ready!");

	// note: must start tokio after forking - it's ub to do a lot of things after forking from a multithreaded process
	let rt = tokio::runtime::Builder::new_current_thread()
		.enable_all()
		.build()
		.context("While building tokio runtime")?;
	let res = rt.block_on(async {
		let mut signalfd = AsyncFd::with_interest(signalfd.raw(), Interest::READABLE).unwrap();
		loop {
			let mut guard = signalfd
				.readable_mut()
				.await
				.context("While waiting for signalfd")?;

			let mut signal = uapi::pod_zeroed::<uapi::c::signalfd_siginfo>();
			let res = uapi::signalfd_read(
				*guard.get_mut().get_mut(),
				std::slice::from_mut(&mut signal),
			)
			.map_err(|e| anyhow::anyhow!(e).context("While reading signalfd"))?;
			if res.is_empty() {
				guard.clear_ready();
				continue;
			}

			match signal.ssi_signo as i32 {
				uapi::c::SIGINT | uapi::c::SIGTERM => {
					return Ok(());
				}
				uapi::c::SIGCHLD => {
					// note: we get sigchld from various executions of `ip`, so we need
					// to gracefully handle that.
					if !child_wait(child_pid, false)? {
						anyhow::bail!("Child unexpectedly exited");
					}
				}
				_ => {
					panic!("Unexpected signal: {:?}", signal);
				}
			}
		}
	});
	std::mem::drop(rt);
	log::info!("Exiting");

	// tell child to terminate
	std::mem::drop(fp);
	if let Err(e) = child_wait(child_pid, true) {
		log::error!("Could not wait for child: {:#}", e);
	}

	res
}

fn child_wait(pid: i32, wait: bool) -> anyhow::Result<bool> {
	loop {
		match uapi::waitpid(
			pid,
			wait.then_some(0).unwrap_or(uapi::c::WNOHANG) | uapi::c::__WALL,
		) {
			Ok((0, _)) => {
				assert!(!wait);
				return Ok(true);
			}
			Ok((rpid, res)) => {
				assert_eq!(rpid, pid);
				if uapi::WIFEXITED(res) {
					let rt = uapi::WEXITSTATUS(res);
					if rt != 0 {
						anyhow::bail!("Child exited with code {}", rt);
					}
					log::debug!("Child exited with code 0");
					return Ok(false);
				} else if uapi::WIFSIGNALED(res) {
					anyhow::bail!("Child exited with signal {}", uapi::WTERMSIG(res))
				}
				panic!("Unrecognized wait status: {:?}", res);
			}
			Err(uapi::Errno(uapi::c::EINTR)) | Err(uapi::Errno(uapi::c::EAGAIN)) => {
				continue;
			}
			Err(uapi::Errno(uapi::c::ECHILD)) => {
				log::debug!("Got ECHILD waiting for child, assuming exited");
				return Ok(false);
			}
			Err(e) => anyhow::bail!(e),
		}
	}
}

fn child(
	mut fp: ForkPipeEnd<C2P, P2C>,
	state_dir: &PathBuf,
	cfg: config::Config,
	addresses: String,
	sockets: Vec<uapi::OwnedFd>,
) -> anyhow::Result<()> {
	// Unshare mounts. See also Notes section in man 7 mount_namespaces
	log::debug!("Child: unsharing mounts");
	uapi::mount("", "/", "", uapi::c::MS_SLAVE | uapi::c::MS_REC, None)
		.context("While unsharing mounts")?;

	// bind-mount new resolv.conf
	log::debug!("Child: Mounting new resolv.conf");
	uapi::mount(
		state_dir.join("resolv.conf"),
		"/etc/resolv.conf",
		"",
		uapi::c::MS_BIND,
		None,
	)
	.context("While bind-mounting /etc/resolv.conf")?;

	// configure loopback
	exec(&[
		"ip",
		"addr",
		"add",
		"dev",
		"lo",
		"127.0.0.1/8",
		"scope",
		"host",
	])?;
	exec(&["ip", "addr", "add", "dev", "lo", "::1", "scope", "host"])?;
	exec(&["ip", "link", "set", "dev", "lo", "up"])?;

	// wait for interface
	log::debug!("Child: Waiting for interface");
	let v = fp.recv()?;
	assert!(matches!(v, P2C::InterfaceMoved));

	// configure addresses
	for ip in addresses.split(',') {
		log::debug!("Child: adding address {:?}", ip);
		exec(&["ip", "addr", "add", "dev", &cfg.interface_name, ip.trim()])?;
	}

	// set interface up
	log::debug!("Child: setting wg interface up");
	exec(&["ip", "link", "set", &cfg.interface_name, "up"])?;

	for veth in cfg.veth.iter() {
		log::debug!("Child: Configuring veth {:?}", veth.container_name);
		exec(&[
			"ip",
			"addr",
			"add",
			"dev",
			&veth.container_name,
			&veth.container_ip.to_string(),
			"scope",
			"link",
			"peer",
			&veth.host_ip.to_string(),
		])?;

		exec(&["ip", "link", "set", "dev", &veth.container_name, "up"])?;
	}

	// add route
	log::debug!("Child: adding route");
	exec(&[
		"ip",
		"route",
		"add",
		"to",
		"default",
		"dev",
		&cfg.interface_name,
	])?;

	// network is now set up

	log::debug!("Child: notifying parent of setup complete");
	fp.send(&C2P::SetupFinished)?;

	// run async stuff
	let rt = tokio::runtime::Builder::new_current_thread()
		.enable_all()
		.build()
		.context("While building tokio runtime")?;
	let res = rt.block_on(async {
		let fp = AsyncFd::new(fp.into_raw_fd()).context("While creating forkpipe async fd")?;
		let (_die_send, die_recv) = tokio::sync::watch::channel(());

		for (socket, pf) in sockets.into_iter().zip(cfg.port_forwards.iter().cloned()) {
			let socket = unsafe {
				tokio::net::TcpListener::from_std(std::net::TcpListener::from_raw_fd(
					socket.unwrap(),
				))
			}
			.context("While creating tokio socket")?;

			let die = die_recv.clone();
			tokio::spawn(async move {
				if let Err(e) = tcp_listen(socket, pf.clone(), die).await {
					log::error!(
						"TCP listener for {} exited with error:\n{}",
						pf.listen_addr(),
						AnyhowErrorFmt(e)
					);
				}
			});
		}

		let _ = fp
			.readable()
			.await
			.context("While waiting for forkpipe readable")?;
		Result::<(), anyhow::Error>::Ok(())
	});
	std::mem::drop(rt);
	log::debug!("Child: exiting");

	// remove interface. stops anything trying to use it.
	exec(&["ip", "link", "delete", "dev", &cfg.interface_name])?;

	res
}

async fn tcp_listen(
	socket: tokio::net::TcpListener,
	pf: PortForward,
	mut die: tokio::sync::watch::Receiver<()>,
) -> anyhow::Result<()> {
	log::debug!("Listener for {:?} starting", pf);
	loop {
		tokio::select! {
			res = die.changed() => {
				if res.is_err() {
					break;
				}
			}
			res = socket.accept() => {
				let (stream, addr) = res.context("While accepting stream")?;
				let pf = pf.clone();
				let die = die.clone();
				tokio::spawn(async move {
					if let Err(e) = tcp_fwd(stream, addr, pf.clone(), die).await {
						log::error!("Forwarder {:?} from {} exited with error:\n{}",
							pf, addr, AnyhowErrorFmt(e));
					}
				});
			}
		}
	}
	log::debug!("Listener for {:?} exiting", pf);
	Ok(())
}

async fn tcp_fwd(
	mut host_stream: tokio::net::TcpStream,
	conn_addr: SocketAddr,
	pf: PortForward,
	mut die: tokio::sync::watch::Receiver<()>,
) -> anyhow::Result<()> {
	let listen_addr = pf.listen_addr();

	let dest_stream = tokio::time::timeout(
		tokio::time::Duration::from_secs(1),
		tokio::net::TcpStream::connect(pf.connect_addr()),
	)
	.await;
	let mut dest_stream = dest_stream
		.context("While connecting to destination")?
		.context("While connecting to destination")?;

	let copy = tokio::io::copy_bidirectional(&mut host_stream, &mut dest_stream);

	tokio::select! {
		res = copy => {
			res.context("While forwarding TCP stream")?;
		}
		res = die.changed() => {
			assert!(res.is_err());
		}
	}

	log::debug!("Forwarder for {} from {} exiting", listen_addr, conn_addr);
	Ok(())
}

//struct DebugStream(tokio::net::TcpStream);
