// TODO: validate suid operation and privilege de-escalation
// TODO: validate namespace security model. ATM, done via UNIX permissions on the state dir and `access` check.

mod config;
mod daemon;
mod forkpipe;
mod ns_exec;

use std::{
	ffi::{
		OsStr,
		OsString,
	},
	io::Read,
	os::fd::FromRawFd,
	path::{
		Path,
		PathBuf,
	},
	process::Command,
};

use anyhow::Context;
use clap::Parser;

#[derive(Parser)]
struct Args {
	#[clap(subcommand)]
	pub subcommand: Subcommand,
}

#[derive(Parser)]
enum Subcommand {
	/// Create a network namespace and start the management daemon
	Daemon {
		/// Namespace name
		name: String,
		/// Path to the namespace configuration
		cfg: PathBuf,
	},
	/// Runs a command in the network namespace
	Exec {
		/// Namespace name
		name: String,
		/// Command to execute and its arguments
		#[clap(required = true, last = true)]
		command: Vec<OsString>,
	},
}

fn main() {
	env_logger::init();

	let args = Args::parse();
	let res = match args.subcommand {
		Subcommand::Daemon { name, cfg } => daemon::daemon(name, cfg),
		Subcommand::Exec { name, command } => ns_exec::ns_exec(name, command),
	};
	if let Err(err) = res {
		eprintln!("Error:\n{}", AnyhowErrorFmt(err));
		std::process::exit(1);
	}
}

fn exec<S: AsRef<OsStr>>(args: &[S]) -> anyhow::Result<()> {
	Command::new(args[0].as_ref())
		.args(&args[1..])
		.spawn()
		.with_context(|| format!("Could not execute {:?}", args[0].as_ref()))?
		.wait()
		.with_context(|| format!("Could not wait {:?}", args[0].as_ref()))?
		.error_for_status()?;
	Ok(())
}

trait ProcError {
	fn error_for_status(self) -> anyhow::Result<()>;
}
impl ProcError for std::process::ExitStatus {
	fn error_for_status(self) -> anyhow::Result<()> {
		if self.success() {
			Ok(())
		} else {
			Err(anyhow::anyhow!("Subprocess exited with code {}", self))
		}
	}
}

fn recursive_delete(path: &Path) {
	match std::fs::read_dir(path) {
		Ok(it) => {
			for entry in it {
				entry
					.and_then(|entry| entry.metadata().map(|md| (entry, md)))
					.map(|(entry, md)| {
						if md.is_dir() {
							recursive_delete(&entry.path());
						} else {
							std::fs::remove_file(&entry.path())
								.map_err(|e| {
									eprintln!("Could not delete {:?}: {}", entry.path(), e)
								})
								.ok();
						}
					})
					.map_err(|e| eprintln!("Could not read {:?}: {}", path, e))
					.ok();
			}
		}
		Err(e) => eprintln!("Could not read {:?}: {}", path, e),
	}
	std::fs::remove_dir(path)
		.map_err(|e| eprintln!("Could not delete {:?}: {}", path, e))
		.ok();
}

fn read_at(fd: i32, path: &Path) -> anyhow::Result<String> {
	let fd = uapi::openat(fd, path, uapi::c::O_RDONLY | uapi::c::O_CLOEXEC, 0)
		.with_context(|| format!("While opening {:?}", path))?;
	let mut f = unsafe { std::fs::File::from_raw_fd(fd.unwrap()) };
	let mut s = String::new();
	f.read_to_string(&mut s)
		.with_context(|| format!("While reading {:?}", path))?;

	Ok(s)
}

pub struct AnyhowErrorFmt(pub anyhow::Error);
impl std::fmt::Display for AnyhowErrorFmt {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		for err in self.0.chain().rev() {
			if let Some(errno) = err.downcast_ref::<uapi::Errno>() {
				let ioerr = std::io::Error::from_raw_os_error(errno.0);
				writeln!(f, "\t- {}", ioerr)?;
			} else {
				writeln!(f, "\t- {}", err)?;
			}
		}
		Ok(())
	}
}
