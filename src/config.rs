use std::{
	ffi::CString,
	net::{
		IpAddr,
		Ipv4Addr,
		SocketAddr,
		SocketAddrV4,
		SocketAddrV6,
	},
	path::PathBuf,
};

use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Config {
	/// Wireguard interface name
	pub interface_name: String,

	/// Path to wireguard config to load, relative to the directory that this
	/// config file is in.
	pub wireguard_cfg: PathBuf,

	/// Owning user of the namespace, either as an integer UID or string user name.
	/// Defaults to zero (ie root).
	///
	/// If installed suid, the program will check if the executing user has access to
	/// the namespace via the standard unix defined here.
	#[serde(deserialize_with = "Config::parse_uid", default)]
	pub uid: u32,

	/// Owning group of the namesapce, either as an integer GID or string group name.
	/// Defaults to zero (ie root).
	///
	/// If installed suid, the program will check if the executing user has access to
	/// the namespace via the standard unix defined here.
	#[serde(deserialize_with = "Config::parse_gid", default)]
	pub gid: u32,

	/// Unix permissions of the namespace, as an integer (octal syntax of `0o###` recommended).
	///
	/// Defaults to zero (ie root only).
	#[serde(default)]
	pub permissions: u32,

	/// Port forwardings.
	#[serde(default)]
	pub port_forwards: Vec<PortForward>,

	/// Virtual ethernet devices to create
	#[serde(default)]
	pub veth: Vec<VEth>,
}
impl Config {
	fn parse_uid<'de, D: serde::Deserializer<'de>>(de: D) -> Result<u32, D::Error> {
		struct Visitor;
		impl<'de> serde::de::Visitor<'de> for Visitor {
			type Value = u32;

			fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
				formatter.write_str("an integer uid or string username")
			}

			fn visit_u32<E>(self, v: u32) -> Result<Self::Value, E>
			where
				E: serde::de::Error,
			{
				Ok(v)
			}

			fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
			where
				E: serde::de::Error,
			{
				let cstr = CString::new(v).map_err(|_| E::custom("invalid username"))?;
				let id = unsafe {
					let ptr = libc::getpwnam(cstr.as_ptr());
					if ptr.is_null() {
						return Err(E::custom(format!("could not find username {:?}", v)));
					}
					(*ptr).pw_uid
				};
				Ok(id)
			}
		}

		de.deserialize_any(Visitor)
	}

	fn parse_gid<'de, D: serde::Deserializer<'de>>(de: D) -> Result<u32, D::Error> {
		struct Visitor;
		impl<'de> serde::de::Visitor<'de> for Visitor {
			type Value = u32;

			fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
				formatter.write_str("an integer gid or string group name")
			}

			fn visit_u32<E>(self, v: u32) -> Result<Self::Value, E>
			where
				E: serde::de::Error,
			{
				Ok(v)
			}

			fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
			where
				E: serde::de::Error,
			{
				let cstr = CString::new(v).map_err(|_| E::custom("invalid groupname"))?;
				let id = unsafe {
					let ptr = libc::getgrnam(cstr.as_ptr());
					if ptr.is_null() {
						return Err(E::custom(format!("could not find username {:?}", v)));
					}
					(*ptr).gr_gid
				};
				Ok(id)
			}
		}

		de.deserialize_any(Visitor)
	}
}

#[derive(Debug, Deserialize, Clone)]
pub struct PortForward {
	pub host_bind: Option<IpAddr>,
	pub host_listen_port: u16,
	pub container_destination_port: u16,
}
impl PortForward {
	pub fn listen_addr(&self) -> SocketAddr {
		match self.host_bind {
			None => SocketAddr::V4(SocketAddrV4::new(
				"127.0.0.1".parse().unwrap(),
				self.host_listen_port,
			)),
			Some(IpAddr::V4(ip)) => SocketAddr::V4(SocketAddrV4::new(ip, self.host_listen_port)),
			Some(IpAddr::V6(ip)) => {
				SocketAddr::V6(SocketAddrV6::new(ip, self.host_listen_port, 0, 0))
			}
		}
	}

	pub fn connect_addr(&self) -> SocketAddr {
		SocketAddr::V4(SocketAddrV4::new(
			"127.0.0.1".parse().unwrap(),
			self.container_destination_port,
		))
	}
}

#[derive(Debug, Deserialize, Clone)]
pub struct VEth {
	pub host_name: String,
	pub container_name: String,
	#[serde(deserialize_with = "de_from_str")]
	pub host_ip: Ipv4Addr,
	#[serde(deserialize_with = "de_from_str")]
	pub container_ip: Ipv4Addr,
}

fn de_from_str<'de, T: std::str::FromStr, D: serde::de::Deserializer<'de>>(
	de: D,
) -> Result<T, D::Error>
where
	T::Err: std::fmt::Display,
{
	use serde::de::Error as _;
	let s = String::deserialize(de)?;
	s.parse::<T>().map_err(|e| D::Error::custom(e.to_string()))
}
