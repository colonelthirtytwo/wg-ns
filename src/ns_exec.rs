use std::{
	ffi::OsString,
	os::{
		fd::AsRawFd,
		unix::prelude::OsStrExt,
	},
	path::Path,
};

use anyhow::Context;

use crate::read_at;

pub fn ns_exec(name: String, args: Vec<OsString>) -> anyhow::Result<()> {
	let cwd = std::env::current_dir().context("While getting cwd")?;

	// open state dir
	let state_dir = Path::new("/run/wg-ns/").join(&name);
	let state_dir_fd = match uapi::open(
		state_dir.as_path(),
		uapi::c::O_RDONLY | uapi::c::O_DIRECTORY | uapi::c::O_CLOEXEC,
		0,
	) {
		Ok(fd) => fd,
		Err(uapi::Errno(uapi::c::ENOENT)) => {
			eprintln!("No namespace named {:?}", name);
			std::process::exit(1);
		}
		Err(e) => return Err(anyhow::anyhow!(e).context("While opening state directory")),
	};

	// check permissions.
	// use faccessat for race condition safety
	match uapi::faccessat(state_dir_fd.as_raw_fd(), ".", uapi::c::R_OK, 0) {
		Ok(()) => {}
		Err(uapi::Errno(uapi::c::EACCES)) => {
			eprintln!("Access denied to namespace {:?}", name);
			std::process::exit(1);
		}
		Err(e) => anyhow::bail!(e),
	}

	// get pid
	let pid = read_at(state_dir_fd.as_raw_fd(), Path::new("ns.pid"))?
		.trim()
		.parse::<i32>()?;
	let pidfd = uapi::pidfd_open(pid, uapi::c::PIDFD_NONBLOCK).context("While opening pidfd")?;

	// move namespaces
	uapi::setns(pidfd.as_raw_fd(), uapi::c::CLONE_NEWNET)?;
	uapi::setns(pidfd.as_raw_fd(), uapi::c::CLONE_NEWNS)?;

	// lower permissions
	let uid = uapi::getuid();
	uapi::setresuid(uid, uid, uid)?;
	let gid = uapi::getgid();
	uapi::setresgid(gid, gid, gid)?;

	// TODO: anything else to lower?

	// restore cwd (if possible)
	std::env::set_current_dir(cwd)
		.map_err(|e| log::warn!("Could not restore cwd: {}", e))
		.ok();

	// exec
	let arg0 = args[0].as_bytes().to_owned();
	let args = args.into_iter().collect::<uapi::UstrPtr>();
	uapi::execvp(arg0, &args).context("While executing")?;
	Ok(())
}
