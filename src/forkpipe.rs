use std::{
	io::Write,
	marker::PhantomData,
	os::unix::prelude::RawFd,
};

use anyhow::Context;
use bincode::Options;
use serde::{
	de::DeserializeOwned,
	Serialize,
};

/// Socket-pair for communicating with a forked process
pub struct ForkPipe<P2C: Serialize + DeserializeOwned, C2P: Serialize + DeserializeOwned> {
	fd1: uapi::OwnedFd,
	fd2: uapi::OwnedFd,
	ph_p2c: PhantomData<fn() -> P2C>,
	ph_c2p: PhantomData<fn() -> C2P>,
}
impl<P2C: Serialize + DeserializeOwned, C2P: Serialize + DeserializeOwned> ForkPipe<P2C, C2P> {
	pub fn new() -> anyhow::Result<Self> {
		let (fd1, fd2) = uapi::socketpair(
			uapi::c::AF_UNIX,
			uapi::c::SOCK_STREAM | uapi::c::SOCK_CLOEXEC,
			0,
		)
		.context("While creating socket pair")?;

		Ok(Self {
			fd1,
			fd2,
			ph_p2c: PhantomData::default(),
			ph_c2p: PhantomData::default(),
		})
	}

	pub fn parent_end(self) -> ForkPipeEnd<P2C, C2P> {
		ForkPipeEnd::new(self.fd1, self.ph_c2p, self.ph_p2c)
	}

	pub fn child_end(self) -> ForkPipeEnd<C2P, P2C> {
		ForkPipeEnd::new(self.fd2, self.ph_p2c, self.ph_c2p)
	}

	#[cfg(test)]
	pub fn both_ends(self) -> (ForkPipeEnd<P2C, C2P>, ForkPipeEnd<C2P, P2C>) {
		(
			ForkPipeEnd::new(self.fd1, self.ph_c2p, self.ph_p2c),
			ForkPipeEnd::new(self.fd2, self.ph_p2c, self.ph_c2p),
		)
	}
}

fn bincode_cfg() -> impl bincode::config::Options {
	bincode::config::DefaultOptions::new()
		.with_native_endian()
		.allow_trailing_bytes()
		.with_fixint_encoding()
}

pub struct ForkPipeEnd<S: Serialize + DeserializeOwned, R: Serialize + DeserializeOwned> {
	fd: uapi::OwnedFd,
	ph_s: PhantomData<fn() -> S>,
	ph_r: PhantomData<fn() -> R>,
}
impl<S: Serialize + DeserializeOwned, R: Serialize + DeserializeOwned> ForkPipeEnd<S, R> {
	fn new(fd: uapi::OwnedFd, ph_r: PhantomData<fn() -> R>, ph_s: PhantomData<fn() -> S>) -> Self {
		Self { fd, ph_r, ph_s }
	}

	pub fn send(&mut self, v: &S) -> anyhow::Result<()> {
		let cfg = bincode_cfg();
		cfg.serialize_into(&mut self.fd, v)?;
		self.fd.flush()?;
		Ok(())
	}

	pub fn recv(&mut self) -> anyhow::Result<R> {
		let cfg = bincode_cfg();

		cfg.deserialize_from(&mut self.fd).map_err(|e| match *e {
			bincode::ErrorKind::Io(e) if e.kind() == std::io::ErrorKind::UnexpectedEof => {
				anyhow::anyhow!("Other process closed communication pipe unexpectedly.")
			}
			e => anyhow::anyhow!(e).context("While receiving message from other process"),
		})
	}

	pub fn _send_fd(&mut self, fd: RawFd) -> anyhow::Result<()> {
		let mut cmsg_buf = [std::mem::MaybeUninit::new(0u8); 512];
		let n = uapi::cmsg_write(
			&mut cmsg_buf.as_mut_slice(),
			uapi::c::cmsghdr {
				cmsg_len: 0,
				cmsg_level: uapi::c::SOL_SOCKET,
				cmsg_type: uapi::c::SCM_RIGHTS,
			},
			&fd,
		)
		.unwrap();

		let iov = [b"\xff".as_slice()];

		let msghdr = uapi::Msghdr {
			iov: iov.as_slice(),
			control: Some(&cmsg_buf[0..n]),
			name: None as Option<&uapi::c::sockaddr>,
		};
		let l = uapi::sendmsg(self.fd.raw(), &msghdr, 0)
			.context("While sendmsg'ing file descriptor")?;
		assert_eq!(l, 1);
		Ok(())
	}

	pub fn _recv_fd(&mut self) -> anyhow::Result<uapi::OwnedFd> {
		let mut buf_contents = [0u8; 1];
		let mut buf_cmsg = [std::mem::MaybeUninit::new(0u8); 512];
		let mut iov = [buf_contents.as_mut_slice()];
		let mut msghdr = uapi::MsghdrMut {
			iov: iov.as_mut_slice(),
			control: Some(buf_cmsg.as_mut_slice()),
			name: None as Option<&mut uapi::c::sockaddr>,
			flags: 0,
		};

		let (iov, _, mut cmsg_slice) =
			uapi::recvmsg(self.fd.raw(), &mut msghdr, 0).context("While receiving message")?;

		assert_eq!(iov.len(), 1);
		let buf = iov.iter().next().unwrap();
		if buf != b"\xff" {
			anyhow::bail!("Expected 0xff read for fd message");
		}

		let (_, cm_header, cm_data) =
			uapi::cmsg_read(&mut cmsg_slice).context("While parsing control message")?;
		if cm_header.cmsg_level != uapi::c::SOL_SOCKET || cm_header.cmsg_type != uapi::c::SCM_RIGHTS
		{
			anyhow::bail!(
				"Expected SCM_RIGHTS control message but got something else: {:?}",
				cm_header
			);
		}

		if cm_data.len() != std::mem::size_of::<i32>() {
			anyhow::bail!(
				"Expected one file descriptor in control message but got a buffer of length {}",
				cm_data.len()
			);
		}

		let mut buf = [0u8; 4];
		buf.copy_from_slice(cm_data);
		let fd = i32::from_ne_bytes(buf);

		Ok(uapi::OwnedFd::new(fd))
	}

	pub fn into_raw_fd(self) -> RawFd {
		self.fd.unwrap()
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[derive(serde::Serialize, serde::Deserialize, PartialEq, Eq, Debug)]
	struct A2B(i32);

	#[derive(serde::Serialize, serde::Deserialize, PartialEq, Eq, Debug)]
	struct B2A(i32);

	#[test]
	fn send_fd() {
		let pipes = ForkPipe::<A2B, B2A>::new().unwrap();
		let (mut a2b, mut b2a) = pipes.both_ends();
		a2b.send(&A2B(123)).unwrap();
		assert_eq!(b2a.recv().unwrap(), A2B(123));

		b2a.send(&B2A(456)).unwrap();
		assert_eq!(a2b.recv().unwrap(), B2A(456));

		let a_fd = uapi::memfd_create("test", 0).unwrap();
		uapi::write(a_fd.raw(), b"test string!").unwrap();
		uapi::lseek(a_fd.raw(), 0, uapi::c::SEEK_SET).unwrap();

		a2b._send_fd(a_fd.raw()).unwrap();

		let b_fd = b2a._recv_fd().unwrap();

		assert_ne!(a_fd, b_fd);

		let mut buf = [0u8; 64];
		let num_read = uapi::read(b_fd.raw(), buf.as_mut_slice()).unwrap();
		assert_eq!(num_read, b"test string!");
	}
}
