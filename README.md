`wg-ns`: Sandbox apps to use a Wireguard VPN
============================================

wg-ns is a tool for running programs inside of a sandbox containing a Wireguard interface and nothing else,
preventing them from using any other connection potentially leaking via other network interfaces. It's meant to be
used along with a VPN to help jail applications to a psudo-anonymous identity.

It handles setting up a network namespace (and mount namespace, see below), configuring Wireguard in it, entering
the namespace, and port forwarding (TCP only at the moment).

Security Caviats
----------------

* This only covers the network side of sandboxing applications. The applications themselves may leak data,
  especially since they share the same view of the filesystem. If that is a concern, consider using more involved
  solutions or a virtual machine.
  * Corollary: Root can break out of the namespaces. This program does not restrict sudo or other elevation methods.
* To operate as conveniently as I wanted this to, this program runs as suid-root, which isn't great from a security
  perspective, especially since it also runs arbitrary user commands afterwards (though after hopefully dropping privileges).
  The security around the namespaces is based off of the namespace state directory's file attributes and the `access`
  call. Review into both of those would be greatly appreciated.

Example usage
-------------

Create a TOML config file in `/etc/wg-ns/`:

```toml
# Name of the wireguard interface. Must not conflict.
interface_name = "mywg"
# Path to the wireguard config file to load, relative to the directory that
# this config file is in. Since this likely has private keys in it, be sure
# that it's not world-readable.
wireguard_cfg = "./myvpn.wg"

# Permissions of the namespace state directory, which contols who can enter it.
# uid/gid may be names or numbers. Anyone with read+list permission on the directory
# can enter the namespace.
uid = "myuser"
gid = "myuser"
permissions = 0o550

# Optional: port forwards from outside the container into the container.
# Currently only TCP forwarding is implemented.
[[port_forwards]]
# IP on the host to listen on. Defaults to localhost only.
#host_bind = "127.0.0.1"

# Host port to listen on
host_listen_port = 5000
# Port in the container to connect to. The container-side connection is
# always bound to localhost.
container_destination_port = 5000

# Optional: Peer-to-peer virtual ethernet devices to set up for host/container configuration.
# See man 4 veth.
[[veth]]
host_name = "testwgh"
container_name = "testwgc"
host_ip = "192.168.240.1"
container_ip = "192.168.240.2"
```

Start the `wg-ns@name-of-file.service` systemctl service, or run manually with
`sudo wg-ns daemon name-of-namespace /path/to/config.toml`.

Once the namespace is set up, run a program in it with `wg-ns exec name-of-namespace -- prog arg1 arg2...`.

Stopping the service or terminating the daemon will delete the Wireguard interface, preventing
any more communication, but currently does not terminate any applications within that namespace.
This may change in the future.

Why not `ip netns`?
-------------------

One main reason: DNS.

While a network namespace alone isolates most of the network configuration, DNS is not included in that - almost all applications
use the DNS servers specified in `/etc/resolv.conf`. To solve this, wg-ns also creates a mount namespace and bind-mounts
`/etc/resolv.conf` with the DNS provider specified in the wireguard config.

Why not `firejail`, Docker, etc.?
---------------------------------

Setting up a Wireguard interface for a sandbox is a bit challenging for these tools, since doing it involes
needing to run commands as root both outside of the container (to create the interface and bind its output
socket to the host namespace, which actually has connectivity) and inside of the container (to set up the
interface and routes). Most tools I've found don't seem to support this - once they let you into the
container, you're privileges have already been dropped.
